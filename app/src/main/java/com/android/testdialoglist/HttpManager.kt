package com.android.testdialoglist

import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object HttpManager {

    private const val mainUrl: String = "api/"

    private val httpClient = createHttpClient()

    fun getRetrofitObject(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(mainUrl)
            .client(httpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun createHttpClient(): OkHttpClient {

        val builder = OkHttpClient.Builder()

        builder
            .addInterceptor(provideHttpLoggerInterceptor())  // To display the request and response details in log

        return builder.build()
    }

    private fun provideHttpLoggerInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            interceptor.level = HttpLoggingInterceptor.Level.BODY
        } else
            interceptor.level = HttpLoggingInterceptor.Level.NONE
        return interceptor
    }
}