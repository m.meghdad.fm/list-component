package com.android.testdialoglist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ViewGroup
import com.android.testdialoglist.base.BaseEndpoint
import com.android.testdialoglist.databinding.ActivityMainBinding
import com.android.testdialoglist.dialoglist.DialogListAdapter
import com.android.testdialoglist.dialoglist.DialogListController
import com.android.testdialoglist.phone.*
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction

class MainActivity : AppCompatActivity() {

    lateinit var binding : ActivityMainBinding
    lateinit var container: ViewGroup

    private lateinit var router : Router


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        container = binding.controllerContainer

        prepareRouter(savedInstanceState)
    }

    private fun prepareRouter(savedInstanceState: Bundle?) {

        // Conductor router
        router = Conductor.attachRouter(this, container, savedInstanceState)

        val endpoint = HttpManager.getRetrofitObject().create(BaseEndpoint::class.java)
        val mapper = PhoneMapper()
        val adapter = DialogListAdapter(
            PhoneViewHolder.Factory()
        )

        val controller = DialogListController<PhoneResponseModel,PhoneViewModel,PhoneViewHolderAction,PhoneViewHolder>(
            loadItemUrl = "phoneMethod",
            numberOfColumns = 1,
            endpoint = endpoint,
            mapper = mapper,
            adapter = adapter
        )

        router.setRoot(RouterTransaction.with(controller))
    }
}
