package visitor.widgets.dialoglist

import io.reactivex.Observable
import com.android.testdialoglist.base.BaseResponseModel
import com.android.testdialoglist.base.BaseViewModel

interface IDialogListController<RM : BaseResponseModel, VM : BaseViewModel> {

    //fun loadData()

    fun getViewState() : Observable<DialogListViewState<RM, VM>>
}