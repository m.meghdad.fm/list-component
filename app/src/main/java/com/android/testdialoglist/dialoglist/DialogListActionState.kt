package com.android.testdialoglist.dialoglist

import com.android.testdialoglist.base.BaseResponseModel
import com.android.testdialoglist.base.BaseViewModel

sealed class DialogListActionState {

    object ListLoadState : DialogListActionState()
    data class ListDataState<VM : BaseViewModel>(val itemsVM: List<VM>? = null) : DialogListActionState()
    data class ListErrorState(val error: Throwable? = null) : DialogListActionState()
    data class ListItemClickState<RM : BaseResponseModel>(val selectedItem: RM? = null) : DialogListActionState()

    object DismissDialogState : DialogListActionState()

    object StableState : DialogListActionState()
}