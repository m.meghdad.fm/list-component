package visitor.widgets.dialoglist

import com.android.testdialoglist.base.BaseResponseModel
import com.android.testdialoglist.base.BaseViewModel
import com.android.testdialoglist.base.BaseViewState
import com.android.testdialoglist.dialoglist.DialogListActionState

data class DialogListViewState<RM : BaseResponseModel, VM : BaseViewModel> (

    val state: DialogListActionState = DialogListActionState.StableState,

    val itemsVM: List<VM>? = null,
    val error: Throwable? = null,
    val selectedItem: RM? = null

) : BaseViewState()



