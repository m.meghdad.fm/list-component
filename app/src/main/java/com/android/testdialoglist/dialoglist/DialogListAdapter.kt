package com.android.testdialoglist.dialoglist

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.testdialoglist.base.BaseVH
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import com.android.testdialoglist.base.BaseViewHolderAction
import com.android.testdialoglist.base.BaseViewModel

class DialogListAdapter<VM : BaseViewModel, VHA : BaseViewHolderAction, VH : BaseVH<VHA, VM>>(
    private val factory : BaseVH.Factory<VH>
) : RecyclerView.Adapter<VH>() {

    private val clickPosition = PublishSubject.create<VHA>()
    private var items : List<VM>? =null

    fun setData(items: List<VM>?) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {

        return factory.createViewHolder(parent, viewType)
    }

    override fun getItemCount(): Int = items?.size ?: 0

    override fun onBindViewHolder(holder: VH, position: Int) {
        items?.elementAt(position)?.let {
            holder.bind(it)
        }

        holder.onItemClick(clickPosition)
    }

    fun getClicks() : Observable<VHA> {
        return clickPosition
            .hide()
    }
}
