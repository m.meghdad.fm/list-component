package com.android.testdialoglist.dialoglist

import com.android.testdialoglist.base.*
import io.reactivex.Observable

class DialogListUseCase<RM : BaseResponseModel, VM : BaseViewModel, VHA : BaseViewHolderAction>(
    private val loadItemUrl: String,
    private val endpoint : BaseEndpoint?,
    private val mapper : BaseMapper<RM, VM>?
) {

    private var itemsRM : List<RM>? = null
    private var itemsVM : List<VM>? = null


    fun stable() : Observable<DialogListActionState> {

        return Observable.just(Unit)
            .map {
                DialogListActionState.StableState
            }
    }

    fun loadList(): Observable<DialogListActionState> {

        endpoint?.let { ep ->
            if (itemsVM.isNullOrEmpty()) {
                return ep.getItems<RM>(loadItemUrl)
                    .map <DialogListActionState>{
                        if (it.isEmpty()) {
                            DialogListActionState.ListErrorState(Exception("received Items is empty"))
                        } else {
                            itemsRM = it
                            mapResponseToViewModel()
                            DialogListActionState.ListDataState(itemsVM)
                        }
                    }
                    .startWith(DialogListActionState.ListLoadState)
                    .onErrorReturn {
                        DialogListActionState.ListErrorState(it)
                    }
            } else {
                return Observable.just(Unit)
                    .map {
                        DialogListActionState.ListDataState(itemsVM)
                    }
            }
        } ?: return Observable.just(Unit).map { DialogListActionState.ListErrorState(Exception("endpoint is  null")) }
    }
    private fun mapResponseToViewModel() {

        itemsVM = mapper?.listMap(itemsRM)
    }

    fun itemClick(vhAction: VHA): Observable<DialogListActionState> {
        val position = vhAction.position

        return Observable.just(Unit)
            .map {
                DialogListActionState.ListItemClickState(itemsRM?.get(position))
            }
    }

    fun dismissDialog(): Observable<DialogListActionState> {
        return Observable.just(Unit)
            .map {
                DialogListActionState.DismissDialogState
            }
    }

}