package com.android.testdialoglist.dialoglist

import com.android.testdialoglist.base.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import visitor.widgets.dialoglist.DialogListViewState
import java.util.concurrent.TimeUnit

class DialogListPresenter<RM : BaseResponseModel, VM : BaseViewModel, VHA : BaseViewHolderAction>(
    val loadItemUrl: String,
    val endpoint : BaseEndpoint?,
    val mapper : BaseMapper<RM, VM>?
) : BasePresenter<DialogListView<RM, VM, VHA>, DialogListViewState<RM, VM>>() {

    val useCase = DialogListUseCase<RM, VM, VHA>(
        loadItemUrl, endpoint, mapper
    )

    override fun bindIntents() {

        val loadList: Observable<DialogListActionState> = intent(DialogListView<RM, VM, VHA>::emitFirstLoad)
            .subscribeOn(Schedulers.io())
            .debounce(200, TimeUnit.MILLISECONDS)
            .doOnNext { Timber.d("bindIntents : loadList") }
            .flatMap {
                useCase.loadList()
            }
            .observeOn(AndroidSchedulers.mainThread())

        val stable: Observable<DialogListActionState> = intent(DialogListView<RM, VM, VHA>::emitStable)
            .subscribeOn(Schedulers.io())
            .debounce(200, TimeUnit.MILLISECONDS)
            .doOnNext { Timber.d("bindIntents : stable") }
            .flatMap {
                useCase.stable()
            }
            .observeOn(AndroidSchedulers.mainThread())

        val itemClick: Observable<DialogListActionState> = intent(DialogListView<RM, VM, VHA>::emitItemClick)
            .subscribeOn(Schedulers.io())
            .debounce(200, TimeUnit.MILLISECONDS)
            .doOnNext { Timber.d("bindIntents : itemClick") }
            .flatMap {
                useCase.itemClick(it)
            }
            .observeOn(AndroidSchedulers.mainThread())

        val dismissDialog: Observable<DialogListActionState> = intent(DialogListView<RM, VM, VHA>::emitDismissDialog)
            .subscribeOn(Schedulers.io())
            .debounce(200, TimeUnit.MILLISECONDS)
            .doOnNext { Timber.d("bindIntents : dismissDialog") }
            .flatMap {
                useCase.dismissDialog()
            }
            .observeOn(AndroidSchedulers.mainThread())

        val allActionState: Observable<DialogListActionState> = Observable.mergeArray(
            loadList,
            itemClick,
            dismissDialog,
            stable
        )

        val initialViewState = DialogListViewState<RM,VM>(state = DialogListActionState.ListLoadState)

        val allViewStates: Observable<DialogListViewState<RM, VM>> = allActionState
            .scan(initialViewState, this::reducer)

        subscribeViewState(allViewStates, DialogListView<RM, VM, VHA>::display)
    }


    private fun reducer(
        previousState: DialogListViewState<RM, VM>,
        currentSate: DialogListActionState
    ): DialogListViewState<RM, VM> {

        Timber.tag("BasicInfo Reducer")

        return when (currentSate) {

            DialogListActionState.ListLoadState -> {
                Timber.d("ListLoadState")
                previousState
                    .copy(
                        state = DialogListActionState.ListLoadState
                    )
            }
            is DialogListActionState.ListDataState<*> -> {
                Timber.d("ListDataState")
                previousState
                    .copy(
                        state = DialogListActionState.ListDataState<VM>(),
                        itemsVM = currentSate.itemsVM as List<VM>
                    )
            }
            is DialogListActionState.ListErrorState -> {
                Timber.d("ListErrorState")
                previousState
                    .copy(
                        state = DialogListActionState.ListErrorState(),
                        error = currentSate.error
                    )
            }
            is DialogListActionState.ListItemClickState<*> -> {
                Timber.d("ListItemSelectedState")
                previousState
                    .copy(
                        state = DialogListActionState.ListItemClickState<RM>(),
                        selectedItem = currentSate.selectedItem as RM
                    )
            }
            DialogListActionState.DismissDialogState -> {
                Timber.d("DismissDialogState")
                previousState
                    .copy(
                        state = DialogListActionState.DismissDialogState
                    )
            }

            DialogListActionState.StableState -> {
                Timber.d("StableState")
                previousState
                    .copy(
                        state = DialogListActionState.StableState
                    )
            }
        }
    }

    public override fun getViewStateObservable(): Observable<DialogListViewState<RM, VM>> {
        return super.getViewStateObservable()
    }
}