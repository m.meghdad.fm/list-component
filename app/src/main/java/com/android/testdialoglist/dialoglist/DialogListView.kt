package com.android.testdialoglist.dialoglist

import io.reactivex.Observable
import com.android.testdialoglist.base.BaseResponseModel
import com.android.testdialoglist.base.BaseView
import com.android.testdialoglist.base.BaseViewHolderAction
import com.android.testdialoglist.base.BaseViewModel
import visitor.widgets.dialoglist.DialogListViewState

interface DialogListView<RM : BaseResponseModel, VM : BaseViewModel, VHA : BaseViewHolderAction> :
    BaseView<DialogListViewState<RM, VM>> {

    fun emitItemClick(): Observable<VHA>
    fun emitDismissDialog() : Observable<Unit>
}