package com.android.testdialoglist.dialoglist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.testdialoglist.base.*
import com.android.testdialoglist.databinding.DialogListBinding
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import visitor.widgets.dialoglist.DialogListViewState
import visitor.widgets.dialoglist.IDialogListController


class DialogListController<RM : BaseResponseModel, VM : BaseViewModel, VHA : BaseViewHolderAction, VH : BaseVH<VHA, VM>>(

    loadItemUrl: String,
    numberOfColumns: Int = 1,
    endpoint: BaseEndpoint?,
    mapper: BaseMapper<RM, VM>?,
    adapter : DialogListAdapter<VM, VHA, VH>?

) : BaseController<DialogListView<RM, VM, VHA>, DialogListViewState<RM, VM>, DialogListPresenter<RM, VM, VHA>>(),
    DialogListView<RM, VM, VHA>,
    IDialogListController<RM, VM> {

    var loadItemUrl: String = ""
    var numberOfColumns: Int = 1
    var endpoint: BaseEndpoint?
    var mapper: BaseMapper<RM, VM>?
    lateinit var adapter : DialogListAdapter<VM, VHA, VH>

    init {
        this.loadItemUrl = loadItemUrl
        this.numberOfColumns = numberOfColumns
        this.endpoint = endpoint
        this.mapper = mapper
        adapter?.let {
            this.adapter = it
        }
    }

    private lateinit var binding: DialogListBinding

    private val loadListSubject = PublishSubject.create<Unit>()
    private val stableSubject = PublishSubject.create<Unit>()

    private var layoutManager: LinearLayoutManager? = null


    val presenter = DialogListPresenter<RM,VM,VHA>(
        loadItemUrl = loadItemUrl,
        endpoint = endpoint,
        mapper = mapper
    )

    override fun createPresenter(): DialogListPresenter<RM, VM, VHA> {
        return presenter
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {

        binding = DialogListBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onAttach(view: View) {
        super.onAttach(view)

        loadListSubject.onNext(Unit)
    }

    /** Emits */

    override fun emitFirstLoad(): Observable<Unit> = loadListSubject
    override fun emitItemClick(): Observable<VHA> = adapter.getClicks()
    override fun emitDismissDialog(): Observable<Unit> = binding.framelayoutDialogListWindowContainer.clicks()
    override fun emitStable(): Observable<Unit> = stableSubject

    // IDialogListController
    override fun getViewState(): Observable<DialogListViewState<RM, VM>> = presenter.viewStateObservable

    /** Display */

    override fun display(vs: DialogListViewState<RM, VM>) {

        binding.progressbarDialogList.visibility = View.GONE

        when(vs.state) {
            DialogListActionState.ListLoadState -> renderLoadList()
            is DialogListActionState.ListDataState<*> -> renderListData(vs.itemsVM)
            is DialogListActionState.ListErrorState -> renderListError(vs.error)
            is DialogListActionState.ListItemClickState<*> -> renderListItemSelectedState()
            DialogListActionState.DismissDialogState -> dismissDialog()
            DialogListActionState.StableState -> renderStable()
        }
    }

    private fun renderLoadList() {
        Timber.d("render : LoadList")

        binding.framelayoutDialogListWindowContainer.visibility = View.VISIBLE

        binding.progressbarDialogList.visibility = View.VISIBLE
        binding.dialoglistTxtviewTitle.text = "Title"

        layoutManager = GridLayoutManager(activity, numberOfColumns)
        binding.dialoglistRecyclerview.layoutManager = layoutManager
        binding.dialoglistRecyclerview.adapter = adapter
    }
    private fun renderListData(items: List<VM>?) {
        Timber.d("render : renderNationalityListData")

        adapter.setData(items)
    }
    private fun renderListError(error: Throwable?) {
        Timber.d("render : renderListError : error = $error")

        // show error on dialog and not dismissed
        //binding.errorpageDialogList.root.visibility = View.VISIBLE

        dismissDialog()
    }
    private fun renderListItemSelectedState() {
        Timber.d("render : NationalityListItemSelected")

        dismissDialog()
    }

    private fun dismissDialog() {
        Timber.d("render : dismissDialog")
        binding.framelayoutDialogListWindowContainer.visibility = View.GONE

        router.popController(this)
    }
}