package com.android.testdialoglist.dialoglist

import com.android.testdialoglist.base.BaseViewModel

data class DialogListRowViewModel (

    val id : String? = null,
    val iconUrl : String? = null,
    val name : String? = null,
    val secondName : String? = null, // ex. native name
    val select : Boolean = false

) : BaseViewModel()