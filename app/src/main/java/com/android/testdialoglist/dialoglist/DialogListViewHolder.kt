package com.android.testdialoglist.dialoglist

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.android.testdialoglist.base.BaseVH
import com.bumptech.glide.Glide
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.row_dialog_list.view.*

class DialogListViewHolder(itemView: View) : BaseVH<DialogListViewHolderAction, DialogListRowViewModel>(itemView) {

    override fun bind(vm: DialogListRowViewModel) {

        if (vm.iconUrl.isNullOrEmpty()) {
            itemView.imgview_row_dialoglist_icon.visibility = View.GONE
        }else{
            itemView.imgview_row_dialoglist_icon.visibility = View.VISIBLE

            Glide.with(itemView)
                .load(vm.iconUrl)
                .into(itemView.imgview_row_dialoglist_icon)
        }

        if (vm.name.isNullOrEmpty()) {
            itemView.txtview_row_dialoglist_name.visibility = View.GONE
        }else {
            itemView.txtview_row_dialoglist_name.visibility = View.VISIBLE
            itemView.txtview_row_dialoglist_name.text = vm.name
        }

        if (vm.secondName.isNullOrEmpty()) {
            itemView.txtview_row_dialoglist_second_name.visibility = View.GONE
        }else{
            itemView.txtview_row_dialoglist_second_name.visibility = View.VISIBLE
            itemView.txtview_row_dialoglist_second_name.text = vm.secondName
        }
    }

    override fun onItemClick(actionSubject: PublishSubject<DialogListViewHolderAction>) {

        itemView.clicks()
            .map { adapterPosition }
            .filter { it != RecyclerView.NO_POSITION }
            .map {
                DialogListViewHolderAction(
                    it
                )
            }
            .subscribe(actionSubject)
    }
}

