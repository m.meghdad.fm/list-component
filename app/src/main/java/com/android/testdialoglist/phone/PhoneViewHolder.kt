package com.android.testdialoglist.phone

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.testdialoglist.R
import com.android.testdialoglist.base.BaseVH
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.row_dialog_list.view.*


class PhoneViewHolder(itemView: View) : BaseVH<PhoneViewHolderAction, PhoneViewModel>(itemView) {

    override fun bind(vm: PhoneViewModel) {

        Glide.with(itemView.context)
            .load(vm.iconUrl)
            .apply(
                RequestOptions()
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
            )
            .into(itemView.imgview_row_dialoglist_icon)

        // name
        if (vm.number.isNullOrEmpty()) {
            itemView.txtview_row_dialoglist_name.visibility = View.GONE
        } else {
            itemView.txtview_row_dialoglist_name.visibility = View.VISIBLE
            itemView.txtview_row_dialoglist_name.text = vm.number
        }
    }

    override fun onItemClick(actionSubject: PublishSubject<PhoneViewHolderAction>) {

        itemView.clicks()
            .map { adapterPosition }
            .filter { it != RecyclerView.NO_POSITION }
            .map {
                PhoneViewHolderAction(
                    it
                )
            }
            .subscribe(actionSubject)
    }

    internal class Factory : BaseVH.Factory<PhoneViewHolder> {
        override fun createViewHolder(parent: ViewGroup?, viewType: Int): PhoneViewHolder {

            val itemView =
                LayoutInflater.from(parent?.context).inflate(R.layout.row_dialog_list, parent, false)
            return PhoneViewHolder(itemView)
        }
    }
}

