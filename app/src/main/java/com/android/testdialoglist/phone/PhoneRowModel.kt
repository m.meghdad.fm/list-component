//package visitor.domain.phone
//
//import android.content.Context
//import android.util.AttributeSet
//import android.view.LayoutInflater
//import android.view.View
//import android.widget.LinearLayout
//import com.Ramamehr.lvacs.R
//import com.Ramamehr.lvacs.databinding.RowPhoneBinding
//import com.airbnb.epoxy.AfterPropsSet
//import com.airbnb.epoxy.CallbackProp
//import com.airbnb.epoxy.ModelProp
//import com.airbnb.epoxy.ModelView
//import com.android.testdialoglist.phone.PhoneViewModel
//import com.bumptech.glide.Glide
//import com.bumptech.glide.Priority
//import com.bumptech.glide.load.engine.DiskCacheStrategy
//import com.bumptech.glide.request.RequestOptions
//
//@ModelView(
//    autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT,
//    saveViewState = true
//)
//class PhoneRowModel @JvmOverloads constructor(
//    context: Context, attributeSet: AttributeSet? = null
//) : LinearLayout(context, attributeSet) {
//    private val binding = RowPhoneBinding.inflate(LayoutInflater.from(context), this, true)
//
//    @ModelProp
//    lateinit var viewModel: PhoneViewModel
//
//    private fun isItemSelected(select: Boolean) {
//
//        if (select) {
//            binding.imgviewSelectIcon.setImageResource(R.drawable.ic_select_filled)
//        } else {
//            binding.imgviewSelectIcon.setImageResource(R.drawable.ic_select_empty)
//        }
//    }
//
//    private fun setPhoneTypeIcon(iconUrl: String?) {
//
//        Glide.with(context)
//            .load(iconUrl)
//            .apply(
//                RequestOptions()
//                    .fitCenter()
//                    .placeholder(R.drawable.ic_cellphone_purple)
//                    .error(R.drawable.ic_cellphone_purple)
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .priority(Priority.NORMAL)
//            )
//            .into(binding.imgviewPhoneIcon)
//    }
//
//    private fun setPhoneTypeText(type: String?) {
//        if (type.isNullOrEmpty()) {
//            binding.txtviewPhoneType.visibility = View.GONE
//        } else {
//            binding.txtviewPhoneType.visibility = View.VISIBLE
//            binding.txtviewPhoneType.text = type
//        }
//    }
//
//    private fun setPhoneNumber(number: String?) {
//        if (number.isNullOrEmpty()) {
//            binding.txtviewPhoneNumber.visibility = View.GONE
//        } else {
//            binding.txtviewPhoneNumber.visibility = View.VISIBLE
//            binding.txtviewPhoneNumber.text = number
//        }
//    }
//
//    @AfterPropsSet
//    fun bindView() {
//
//        isItemSelected(viewModel.select)
//        setPhoneTypeIcon(viewModel.type?.iconUrl)
//        setPhoneTypeText(viewModel.type?.name)
//        setPhoneNumber(viewModel.number)
//    }
//
//    @CallbackProp
//    fun listener(listener: ((rowAction: PhoneRowActionModel) -> Boolean)?) {
//        if (listener == null) {
//            binding.root.setOnClickListener(null)
//            binding.linearlayoutPhoneEditContainer.setOnClickListener(null)
//        } else {
//            binding.root.setOnClickListener {
//                listener(PhoneRowActionModel.ItemClick(viewModel))
//            }
//            binding.linearlayoutPhoneEditContainer.setOnClickListener {
//                listener(PhoneRowActionModel.EditPhoneClick(viewModel))
//            }
//        }
//    }
//}
