package com.android.testdialoglist.phone

import com.android.testdialoglist.base.BaseViewModel


data class PhoneViewModel(

    val id: String? = null,
    val number: String? = null,
    val iconUrl : String? = null,
    var select: Boolean = false

) : BaseViewModel()