package com.android.testdialoglist.phone

import com.android.testdialoglist.base.BaseMapper

class PhoneMapper : BaseMapper<PhoneResponseModel, PhoneViewModel>() {

    override fun map(responseItem: PhoneResponseModel?): PhoneViewModel? {

        if (responseItem == null)
            return null

        return PhoneViewModel(
            id = responseItem.id,
            iconUrl = responseItem.iconUrl,
            number = responseItem.number
        )
    }
}