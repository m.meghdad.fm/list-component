package com.android.testdialoglist.phone

import com.android.testdialoglist.base.BaseResponseModel
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PhoneResponseModel (

    @SerializedName("gid")
    val id : String? = null,

    @SerializedName("icon_url")
    val iconUrl : String? = null,

    @SerializedName("number")
    val number : String? = null

) : BaseResponseModel()
