package com.android.testdialoglist.base

import com.hannesdorfmann.mosby3.mvp.MvpView
import io.reactivex.Observable

interface BaseView<VS : BaseViewState> : MvpView {

    fun emitFirstLoad() : Observable<Unit>
    fun emitStable(): Observable<Unit>

    fun display(vs: VS)
}