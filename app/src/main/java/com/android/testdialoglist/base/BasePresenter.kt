package com.android.testdialoglist.base

import com.hannesdorfmann.mosby3.mvi.MviBasePresenter

abstract class BasePresenter<V : BaseView<VS>, VS : BaseViewState> : MviBasePresenter<V, VS>()