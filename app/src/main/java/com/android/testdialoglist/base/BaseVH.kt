package com.android.testdialoglist.base

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.subjects.PublishSubject


abstract class BaseVH<VHA : BaseViewHolderAction, VM : BaseViewModel>(itemView : View) : RecyclerView.ViewHolder(itemView) {

    interface Factory<VH> {
        fun  createViewHolder(parent: ViewGroup?, viewType: Int): VH
    }

    abstract fun bind(vm : VM)

    abstract fun onItemClick(actionSubject: PublishSubject<VHA>)
}
