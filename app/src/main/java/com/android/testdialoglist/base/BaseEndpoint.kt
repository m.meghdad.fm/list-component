package com.android.testdialoglist.base

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Url


interface BaseEndpoint  {

    @GET
    fun<RM : BaseResponseModel> getItems (@Url url : String): Observable<List<RM>>
}
