package com.android.testdialoglist.base

import android.view.ViewGroup
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.hannesdorfmann.mosby3.MviController
import timber.log.Timber

abstract class BaseController<V : BaseView<VS>, VS : BaseViewState, PR : BasePresenter<V, VS>> :
    MviController<V, PR>() {

    fun addChild(resID: Int, controller: Controller) {

        val container = view?.findViewById(resID) as? ViewGroup

        container?.let {
            val router = getChildRouter(it).setPopsLastView(false)

            router.setRoot(
                RouterTransaction.with(controller)
                    .pushChangeHandler(FadeChangeHandler())
                    .popChangeHandler(FadeChangeHandler())
            )
        }
    }

    fun renderStable() {
        Timber.d("render : Stable")
    }
}