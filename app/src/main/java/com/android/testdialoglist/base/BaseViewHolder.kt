package com.android.testdialoglist.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.subjects.PublishSubject


abstract class BaseViewHolder<A : BaseViewHolderAction, K>(itemView : View) :
   RecyclerView.ViewHolder(itemView) {

   abstract fun bind(vm : K)

   abstract fun onItemClick(actionSubject: PublishSubject<A>)
}