package com.android.testdialoglist.base

abstract class BaseMapper<RM : BaseResponseModel, VM : BaseViewModel> {

    abstract fun map(responseItem : RM?) : VM?

    fun listMap(responseItems: List<RM>?): List<VM>? {

        if (responseItems == null)
            return null

        val viewItems: MutableList<VM> = mutableListOf()

        responseItems.forEach { rm ->
            map(rm)?.let { vm ->
                viewItems.add(vm)
            }
        }

        return viewItems.toList()
    }
}