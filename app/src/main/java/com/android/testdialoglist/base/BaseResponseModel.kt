package com.android.testdialoglist.base

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
open class BaseResponseModel : Parcelable
