package com.android.testdialoglist.base

import io.reactivex.Observable

interface BaseIController<VS : BaseViewState> {
    fun getViewState() : Observable<VS>
}
